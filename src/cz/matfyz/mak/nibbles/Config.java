package cz.matfyz.mak.nibbles;

import java.awt.*;

/**
 * main configuration and global parameters
 *
 *@author     Martin Kvapil <mak@matfyz.cz>
 *@created    16. september 2001
 */
public class Config {
	
	public static final int NORTH = 1;
	public static final int WEST = 2;
	public static final int SOUTH = 3;
	public static final int EAST = 4;
	public static final int LEFT = 1;
	public static final int RIGHT = -1;
	
	public static final int width = 30;
	public static final int height = 20;
	public static final int nibbleSize = 20;
	public static final Color backgroundColor = new Color(0, 0, 0);
	public static final Color borderColor = new Color(255, 255, 255);
	public static final Color foodColor = new Color(255, 200, 0);

	public static final int maxPlayers = 4;
	public static final int lives = 3;
	public static final int length = 5;
	public static final int speed = 150;
	
	public Color color;
	public int leftKey;
	public int rightKey;
	
	Config(int red, int green, int blue, int aLeftKey, int aRightKey) {
		color = new Color(red, green, blue);
		leftKey = aLeftKey;
		rightKey = aRightKey;
	}
	
}

