package cz.matfyz.mak.nibbles;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

/**
 * board painting, game timer handling and key handling
 *
 *@author     Martin Kvapil <mak@matfyz.cz>
 *@created    16. september 2001
 */
public class Game extends JPanel implements ActionListener, KeyListener {
	
	/**
	 * initial position and direction structure for each level
	 */
	public class Start {
		public int x, y;
		public int direction;
		public Start(int ax, int ay, int aDirection) {
			x = ax;
			y = ay;
			direction = aDirection;
		}
	}
	
	private Nibbles parrent;
	
	private int width;
	private int height;
	private int nibbleSize;
	private Color backgroundColor;
	private Color borderColor;
	private Color foodColor;

	/**
	 * -2 ... food
	 * -1 ... boarders
	 *  0 ... empty
	 * >0 ... nibble
	 */
	private int level[][];
	private Config config[];
	private Player nib[];
	private int count = 0;

	private int curLevel;
	private int foodValue;
	private int countdown;
	
	private ScoreTable score;
	private Timer timer;
	private java.util.Random random;
	
	/**
	 * creates and initializes game board
	 *
	 *@param aParent pointer at parrent for changing menu
	 *@param aScore ScoreTable
	 */
	public Game(Nibbles aParrent, ScoreTable aScore) {
		super();
		parrent = aParrent;
		score = aScore;
		width = Config.width;
		height = Config.height;
		nibbleSize = Config.nibbleSize;
		backgroundColor = Config.backgroundColor;
		borderColor = Config.borderColor;
		foodColor = Config.foodColor;
		level = new int[width+2][height+2];
		
		config = new Config[Config.maxPlayers];
		config[0] = new Config(255, 0, 0, KeyEvent.VK_LEFT, KeyEvent.VK_RIGHT);
		config[1] = new Config(0, 0, 255, KeyEvent.VK_Z, KeyEvent.VK_X);
		config[2] = new Config(0, 255, 0, KeyEvent.VK_O, KeyEvent.VK_P);
		config[3] = new Config(0, 255, 255, KeyEvent.VK_Q, KeyEvent.VK_W);
		
		setPreferredSize(new Dimension((width+2)*nibbleSize, (height+2)*nibbleSize));
	}

	/**
	 * loads level, creates nibbles and starts the game
	 */
	public void start(int aCount, int speed) {
		int i;
		
		Start[] start = loadLevel(1);
		
		count = aCount;
		nib = new Player[aCount];
		for (i = 0; i < count; i ++) {
			nib[i] = new Player(Config.lives);
			if (nib[i].restartAt(start[i].x, start[i].y, start[i].direction, Config.length)) {
				setCell(start[i].x, start[i].y, i+1);
			}
			
			score.setScore(i, 0, nib[i].getLives());
		}
		
		for (i = count; i < Config.maxPlayers; i ++) {
			score.setScore(i, 0, 0);
		}
		
		newFood();
		
		if (timer != null) {
			if (timer.isRunning()) timer.stop();
			timer.setDelay(speed);
		}
		else {
			timer = new Timer(speed, this);
		}
		timer.start();
	}
	
	/**
	 * stops current game
	 */
	public void stop() {
		count = 0;
		if (timer.isRunning()) {
			timer.stop();
			timer = null;
		}
		repaint();
	}

	/**
	 * pause / unpause current game
	 */
	public void pause() {
		if (timer.isRunning()) {
			timer.stop();
		}
		else {
			timer.start();
		}
	}
	
	/**
	 * Loads level borders and start positions and directions of nibbles
	 *
	 *@param aLevel number of the level
	 */
	private Start[] loadLevel(int aLevel) {
		int i, j;
		
		curLevel = aLevel;
		for (i = 0; i < height+2; i ++) {
			level[0][i] = -1;
			level[width+1][i] = -1;
		}
		for (i = 1; i < width+1; i ++) {
			level[i][0] = -1;
			level[i][height+1] = -1;
			for (j = 1; j < height+1; j ++) {
				level[i][j] = 0;
			}
		}
		
		random = new java.util.Random();
		foodValue = 0;
		countdown = 29;
		
		// initial positions and directions
		Start[] start = new Start[Config.maxPlayers];
		start[0] = new Start(8, height/2, Config.WEST);
		start[1] = new Start(width-7, height/2, Config.EAST);
		start[2] = new Start(width/2, 8, Config.NORTH);
		start[3] = new Start(width/2, height-7, Config.SOUTH);
		
		return start;
	}
	
	/**
	 * Generetes new position for food
	 */
	private void newFood() {
		int x, y;
		
		foodValue += 1;
		do {
			x = 1 + random.nextInt(width + 1);
			y = 1 + random.nextInt(height + 1);
		} while (level[x][y] != 0);
		level[x][y] = -2;
	}

	/**
	 * Sets nibble cell to position [x, y]
	 */
	private void setCell(int x, int y, int who) {
		level[x][y] = who;	// nibbles will be numbered from 0 => add 1
	}

	/**
	 * Clears nibble cell from position [x, y]
	 */
	private void clearCell(int x, int y) {
		level[x][y] = 0;
	}

	/**
	 * timer event - move nibbles and check for collisions
	 */
	public void actionPerformed(ActionEvent e) {
		int i, j, x, y;
		boolean playing, nofood, collision;
		
		if (countdown-- > 0) {
			// test if somebody is playing
			playing = false;
			for (i = 0; i < count; i ++) {
				if (!nib[i].isPlaying()) continue;
				playing = true;
			}
			if (!playing) parrent.gameStop();
			
			repaint();
			return;
		}

		// clear tails and move nibble
		for (i = 0; i < count; i ++) {
			clearCell(nib[i].getTailX(), nib[i].getTailY());
			nib[i].move();
		}
		
		nofood = collision = false;
		
		// check for collision with board and other nibble body
		for (i = 0; i < count; i ++) {
			x = nib[i].getHeadX();
			y = nib[i].getHeadY();
			if (!nib[i].isPlaying() || level[x][y] == 0) continue;
			switch (level[x][y]) {
				case -2: // got food
					nib[i].addScore(foodValue);
					nofood = true;
					// if the level is complete, start next
					if (foodValue >= 9) {
						Start[] start = loadLevel(curLevel + 1);
						for(j = 0; j < count; j ++) {
							if (nib[j].restartAt(start[j].x, start[j].y,
									start[j].direction, config.length)) {
								setCell(start[j].x, start[j].y, i+1);
							}
						}
						timer.setDelay((int)(timer.getDelay()*0.80));
					}
					break;
				case -1: // collision with boarders
					nib[i].failure();
					collision = true;
					break;
				default: // collision with other nibble
					nib[i].failure();
					collision = true;
					break;
			}
			score.setScore(i, nib[i].getScore(), nib[i].getLives());
		}
		// check for collision with head of othe nibble
		for (i = 0; i < count; i ++) {
			if (!nib[i].isPlaying()) continue;
			x = nib[i].getHeadX();
			y = nib[i].getHeadY();
			for (j = i + 1; j < count; j ++) {
				if (!nib[j].isPlaying()) continue;
				if (x == nib[j].getHeadX() && y == nib[j].getHeadY()) {
					nib[i].failure();
					nib[j].failure();
					score.setScore(i, nib[i].getScore(), nib[i].getLives());
					score.setScore(j, nib[j].getScore(), nib[j].getLives());
					collision = true;
				}
			}
		}
		// set heads to new positions on board
		for (i = 0; i < count; i ++) {
			if (!nib[i].isPlaying()) continue;
			setCell(nib[i].getHeadX(), nib[i].getHeadY(), i+1);
		}
		
		// if there was some collision
		if (collision) {
			Start[] start = loadLevel(curLevel);
			for (i = 0; i < count; i ++) {
				if (nib[i].restartAt(start[i].x, start[i].y, start[i].direction, config.length)) {
					setCell(start[i].x, start[i].y, i+1);
				}
			}
			nofood = true;
		}
		
		if (nofood) newFood();
		repaint();
	}

	/**
	 * redraws board and all nibbles
	 */
	public void paintComponent(Graphics g) {
		int i, j;
		
		super.paintComponent(g);

		for (i = 0; i < width+2; i ++) {
			for (j = 0; j < height+2; j ++) {
				switch (level[i][j]) {
					case -1:
						g.setColor(borderColor);
						break;
					case -2:
					case 0:
						g.setColor(backgroundColor);
						break;
					default:
						g.setColor(config[level[i][j]-1].color);
						break;
				}
				g.fillRect(i*nibbleSize, j*nibbleSize, nibbleSize, nibbleSize);

				if (level[i][j] == -2) {
					int a = nibbleSize/2 - 4;
					g.setColor(foodColor);
					g.fillOval(i*nibbleSize, j*nibbleSize, nibbleSize, nibbleSize);
					g.setColor(backgroundColor);
					g.drawString(new Integer(foodValue).toString(), i*nibbleSize+a, (j+1)*nibbleSize-a);
				}
			}
		}
		if (countdown > 0 && timer != null && timer.isRunning()) {
			g.drawString("Level "+curLevel+" will start in " + (1+countdown/10) +
					((countdown==1)?" second":" seconds."),
				(width/2-10)*nibbleSize, 2*nibbleSize);
		}
	}
	
	/**
	 * keyboard event
	 */
	public void keyPressed(KeyEvent e) {
		int i, code;
		
		if (countdown > 0) return;
		
		for (i = 0; i < count; i ++) {
			code = e.getKeyCode();
			if (code == config[i].leftKey) {
				nib[i].turn(Config.LEFT);
			}
			else if (code == config[i].rightKey) {
				nib[i].turn(Config.RIGHT);
			}
		}
	}
	public void keyReleased(KeyEvent e) {}
	public void keyTyped(KeyEvent e) {}
	
}

