package cz.matfyz.mak.nibbles;

import java.awt.*;
import javax.swing.*;

/**
 * displayes score and lives of all players
 *
 *@author     Martin Kvapil <mak@matfyz.cz>
 *@created    16. september 2001
 */
public class ScoreTable extends JPanel {
	
	private int count;
	private JLabel score[];
	
	public ScoreTable(int aCount) {
		JLabel label;
		count = aCount;
		setLayout(new GridLayout(1, 9));
		score = new JLabel[count];
		for (int i = 1; i <= count; i ++) {
			label = new JLabel("   Player "+i+": ");
			add(label);
			score[i-1] = new JLabel(0 + " / " + Config.lives);
			add(score[i-1]);
		}
	}
	
	public void reset() {
		for (int i = 0; i < count; i ++)
			setScore(i, 0, Config.lives);
	}
	
	public void setScore(int who, int points, int lives) {
		score[who].setText(points + " / " + lives);
	}
	
}

