package cz.matfyz.mak.nibbles;

import java.util.*;
import java.awt.*;

/**
 * Nibbles handling
 *
 *@author     Martin Kvapil <mak@matfyz.cz>
 *@created    16. september 2001
 */
public class Player {
	
	private Vector body;
	private int direction;
	private int length;
	private int size;
	private int score, lives;
	private boolean playing, failed, wasTurned;
	
	/**
	 * Constructs and initializes nibble before game's start
	 */
	public Player(int aLives) {
		lives = aLives;
		score = 0;
		playing = true;
	}
	
	/**
	 * initializes nibble position, direction and length
	 */
	public boolean restartAt(int x, int y, int aDirection, int aLength) {
		if (lives > 0) {
			body = new Vector();
			body.add(new Point(x, y));
			direction = aDirection;
			length = aLength;
			wasTurned = failed = false;
		}
		else {
			playing = false;
		}
		return playing;
	}
	
	/**
	 * Moves nibble
	 */
	public void move() {
		if (!playing) return;
		
		int x = getHeadX();
		int y = getHeadY();
		
		switch (direction) {
			case Config.NORTH:
				y -= 1;
				break;
			case Config.WEST:
				x -= 1;
				break;
			case Config.SOUTH:
				y += 1;
				break;
			case Config.EAST:
				x += 1;
				break;
		}
		if (body.size() < length) {
			body.add(new Point(x, y));
		}
		else {
			Point p = (Point) body.firstElement();
			body.remove(p);
			p.move(x, y);
			body.add(p);
		}
		wasTurned = failed = false;
	}
	
	/**
	 * Sets nibble direction
	 */
	public void turn(int where) {
		if (!playing || wasTurned) return;
		
		direction += where;
		if (direction < 1) direction += 4;
		if (direction > 4) direction -= 4;
		wasTurned = true;
	}
	
	/**
	 * player failed
	 */
	public void failure() {
		if (playing && !failed) {
			failed = true;
			lives -= 1;
		}
	}
	
	/**
	 * Tells is the player is playing
	 */
	public boolean isPlaying() {
		return playing;
	}
	
	/**
	 * Returns how many lives does the player have
	 */
	public int getLives() {
		return lives;
	}
	
	/**
	 * adds point to nibble and prolonges nibble body
	 */
	public void addScore(int pt) {
		score += pt;
		length += 3 + pt*2;
	}
	
	/**
	 * Returns players score
	 */
	public int getScore() {
		return score;
	}
	
	/**
	 * Returns x coord of nibble head
	 */
	public int getHeadX() {
		return (int) ((Point) body.lastElement()).getX();
	}
	/**
	 * Returns y coord of nibble head
	 */
	public int getHeadY() {
		return (int) ((Point) body.lastElement()).getY();
	}
	/**
	 * Returns x coord of nibble tail
	 */
	public int getTailX() {
		return (int) ((Point) body.firstElement()).getX();
	}
	/**
	 * Returns y coord of nibble tail
	 */
	public int getTailY() {
		return (int) ((Point) body.firstElement()).getY();
	}
	
}

