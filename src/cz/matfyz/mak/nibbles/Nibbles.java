package cz.matfyz.mak.nibbles;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

/**
 *  Main class handling menu commands
 *
 *@author     Martin Kvapil <mak@matfyz.cz>
 *@created    16. september 2001
 */
public class Nibbles extends JFrame {
	
	private JMenuItem gameNewItem = null;
	private JMenuItem gameStopItem = null;
	private JMenuItem gamePauseItem = null;
	private JMenuItem gameExitItem = null;
	private JMenuItem pl1Item = null;
	private JMenuItem pl2Item = null;
	private JMenuItem pl3Item = null;
	private JMenuItem pl4Item = null;
	private JMenuItem helpHelpItem = null;
	private JMenuItem helpAboutItem = null;
	
	MenuHandler menuHandler = new MenuHandler();
	
	private int playerCnt;
	private boolean playing = false;
	private boolean paused = false;
	
	private ScoreTable score;
	private Game game;
	
	/**
	 *  Constructor for the Nibbles object
	 */
	public Nibbles() {
		super("Nibbles for Java");

		JMenuBar menuBar;
		JMenu menu;

		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				System.exit(0);
			}
		});

		gameNewItem = new JMenuItem("New", KeyEvent.VK_N);
		gameNewItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F2, 0));
		gameNewItem.addActionListener(menuHandler);

		gameStopItem = new JMenuItem("Stop", KeyEvent.VK_S);
		gameStopItem.addActionListener(menuHandler);

		gamePauseItem = new JMenuItem("Pause", KeyEvent.VK_P);
		gamePauseItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F3, 0));
		gamePauseItem.addActionListener(menuHandler);

		gameExitItem = new JMenuItem("Exit", KeyEvent.VK_X);
		gameExitItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_X, Event.ALT_MASK));
		gameExitItem.addActionListener(menuHandler);
		
		pl1Item = new JMenuItem("One player", KeyEvent.VK_1);
		pl1Item.addActionListener(menuHandler);
		pl2Item = new JMenuItem("Two players", KeyEvent.VK_2);
		pl2Item.addActionListener(menuHandler);
		pl3Item = new JMenuItem("Three players", KeyEvent.VK_3);
		pl3Item.addActionListener(menuHandler);
		pl4Item = new JMenuItem("Four players", KeyEvent.VK_4);
		pl4Item.addActionListener(menuHandler);

		helpHelpItem = new JMenuItem("Help", KeyEvent.VK_H);
		helpHelpItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F1, 0));
		helpHelpItem.addActionListener(menuHandler);

		helpAboutItem = new JMenuItem("About", KeyEvent.VK_A);
		helpAboutItem.addActionListener(menuHandler);

		menuBar = new JMenuBar();
		setJMenuBar(menuBar);

		menu = new JMenu("Game");
		menu.setMnemonic(KeyEvent.VK_G);
		menu.add(gameNewItem);
		menu.add(gameStopItem);
		menu.add(gamePauseItem);
		menu.addSeparator();
		menu.add(gameExitItem);
		menuBar.add(menu);
		
		menu = new JMenu("Players");
		menu.setMnemonic(KeyEvent.VK_P);
		menu.add(pl1Item);
		menu.add(pl2Item);
		menu.add(pl3Item);
		menu.add(pl4Item);
		menuBar.add(menu);
		
		menu = new JMenu("Help");
		menu.setMnemonic(KeyEvent.VK_H);
		menu.add(helpHelpItem);
		menu.add(helpAboutItem);
		menuBar.add(menu);

		gameStopItem.setEnabled(false);
		gamePauseItem.setEnabled(false);

		Container contentPane = getContentPane();
		
		score = new ScoreTable(Config.maxPlayers);
		contentPane.add(score, BorderLayout.NORTH);
		game = new Game(this, score);
		contentPane.add(game, BorderLayout.CENTER);
		
		playerCnt = 1;
	}
	
	/**
	 *  modifies menu and starts the game
	 */
	public void gameNew() {
		if (!playing || paused) {
			addKeyListener(game);
		}
		game.start(playerCnt, Config.speed);
		
		playing = true;
		paused = false;
		gamePauseItem.setText("Pause");
		gamePauseItem.setEnabled(true);
		gameStopItem.setEnabled(true);
		
	}
	
	/**
	 *  modifies menu and stops the game
	 */
	public void gameStop() {
		if (playing) {
			removeKeyListener(game);
			game.stop();
		}
		
		playing = false;
		paused = false;
		gamePauseItem.setText("Pause");
		gamePauseItem.setEnabled(false);
		gameStopItem.setEnabled(false);
	}
	
	/**
	 *  pauses the game
	 */
	void gamePause() {
		if (!playing) return;
		paused = !paused;
		if (paused) {
			removeKeyListener(game);
			gamePauseItem.setText("Resume");
			gamePauseItem.setMnemonic(KeyEvent.VK_R);
		}
		else {
			addKeyListener(game);
			gamePauseItem.setText("Pause");
			gamePauseItem.setMnemonic(KeyEvent.VK_P);
		}
		game.pause();
	}
	
	/**
	 *  exits the game
	 */
	void gameExit() {
		this.dispose();
		System.exit(0);
	}
	
	/**
	 * one player will be playing
	 */
	void OnePlayer() {
		playerCnt = 1;
	}
	
	/**
	 * two players will be playing
	 */
	void TwoPlayers() {
		playerCnt = 2;
	}
	
	/**
	 * three players will be playing
	 */
	void ThreePlayers() {
		playerCnt = 3;
	}
	
	/**
	 * four players will be playing
	 */
	void FourPlayers() {
		playerCnt = 4;
	}
	
	/**
	 *  key configuration help
	 */
	void helpHelp() {
	}

	/**
	 *  about the game
	 */
	void helpAbout() {
	}
	
	/**
	 *  The main program for the Nibbles class
	 *
	 *@param  args  The command line arguments
	 */
	public static void main(String[] args) {
		try {
			UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());
		}
		catch (Exception e) {
		}

		Nibbles app = new Nibbles();
		app.pack();
		app.setResizable(false);
		app.setVisible(true);
	}
	
	/**
	 *  handling of the menu commands
	 */
	class MenuHandler implements ActionListener {

		public void actionPerformed(ActionEvent e) {
			if (e.getSource() == Nibbles.this.gameNewItem) {
				gameNew();
			}
			if (e.getSource() == Nibbles.this.gameStopItem) {
				gameStop();
			}
			if (e.getSource() == Nibbles.this.gamePauseItem) {
				gamePause();
			}
			if (e.getSource() == Nibbles.this.gameExitItem) {
				gameExit();
			}
			if (e.getSource() == Nibbles.this.pl1Item) {
				OnePlayer();
			}
			if (e.getSource() == Nibbles.this.pl2Item) {
				TwoPlayers();
			}
			if (e.getSource() == Nibbles.this.pl3Item) {
				ThreePlayers();
			}
			if (e.getSource() == Nibbles.this.pl4Item) {
				FourPlayers();
			}
			if (e.getSource() == Nibbles.this.helpHelpItem) {
				helpHelp();
			}
			if (e.getSource() == Nibbles.this.helpAboutItem) {
				helpAbout();
			}
		}
	}
	
}

